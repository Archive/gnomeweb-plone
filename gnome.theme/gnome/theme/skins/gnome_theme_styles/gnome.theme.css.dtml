/*
  This file is based on the ploneCustom.css.dtml shipped with Plone.
*/

/*  <dtml-with base_properties> (do not remove this :) */
/*  <dtml-call "REQUEST.set('portal_url', portal_url())"> (not this either :) */

s
body
{
  background: #DEE1DB;
}

/*** Begin: General Navigation ***/
#general
{
  list-style: none;
  background: #2E3436 url(&dtml-portal_url;/general_bg.png) 0 100% repeat-x;
  text-align: right;
  padding: 0 1ex;
}

#general li /* For non-home links, <li> is display: inline; with text-align: right on the <ul>. */
{
  display: inline;
  background: url(&dtml-portal_url;/general_separator.png) 0 0 no-repeat;
  padding-top: 10px;
  padding-bottom: 8px;
}

#general li a
{
  font-weight: bold;
  color: #FFFFFF;
  margin: 0 2ex;
  text-decoration: none;
  line-height: 30px;
}

#general li a:hover
{
  text-decoration: underline;
}

#general .home /* The home link is specifically floated left */
{
  float: left;
  background: url(&dtml-portal_url;/general_separator.png) 100% 0 no-repeat;
  padding-top: 0;
  padding-bottom: 0;
}

#general .home a
{
  float: left; /* This is necessary for the foot not to be cut off */
  background: url(&dtml-portal_url;/foot.png) 7px 50% no-repeat;
  margin-left: 0;
  padding-left: 27px;
}
/*** End: General Navigation ***/

/*** Begin: Header ***/
#header
{
  background: #729FCF url(&dtml-portal_url;/logo.png) 3ex 24% no-repeat;
  height: 1%;
  float: left;
  width: 100%;
}

h1
{
  text-indent: -9999px;
  overflow: hidden;
  /* This is far from an ideal solution (the images/CSS on case fails, and screen readers probably skip this */
  display: none;
}

#tabs
{
  background: url(&dtml-portal_url;/bar.png) 0 100% repeat-x; /* 2px #CCCCCC bottom border of the tab bar */
  width: 100%;
  float: left;
}

#portal-globalnav
{
  float: right;
  list-style: none;
  margin-right: 3ex;
}

#portal-globalnav li
{
  float: left;
  margin-left: 0.2ex;
  font-size: 2ex;
}


#portal-globalnav li a:hover
{
  color: #111111;
}

#portal-globalnav li a /* Based off of http://alistapart.com/articles/slidingdoors2/ */
{
  float: left;
  text-decoration: none;
  color: #555555;
  background: url(&dtml-portal_url;/tab_left.png) 0 0 no-repeat;
  padding: 7px 0 7px 7px;
  border-bottom: 2px solid #CCCCCC;
}

#portal-globalnav li span
{
  background: url(&dtml-portal_url;/tab_right.png) 100% 0 no-repeat;
  padding: 7px 28px 7px 19px;
}

#portal-globalnav li.selected a,
{
  color: #3566A5;
  background: url(&dtml-portal_url;/tab_left.png) 0 -57px no-repeat;
  border-bottom: none;
  padding-top: 8px;
  padding-bottom: 8px;
}

#portal-globalnav li.selected span,
{
  background: url(&dtml-portal_url;/tab_right.png) 100% -57px no-repeat;
  padding-top: 8px;
  padding-bottom: 8px;
}

#portal-secondnav
{
  background: url(&dtml-portal_url;/secondary_bg.png) 0 0 repeat-x;
  padding: 0.25ex 3ex;
  border-bottom: 1px solid #CCCCCC;
  list-style: none;
  clear: both;
}

#portal-secondnav li
{
  display: inline;
  border-right: 1px solid #CCCCCC;
}

#portal-secondnav li.first
{
  border-left: 1px solid #CCCCCC;
}

#portal-secondnav li a
{
  text-decoration: none;
  color: #335588;
  margin: 0 3ex;
}

#portal-secondnav li a:hover
{
  color: #000000;
}

#control
{
  text-align: right;
  position: relative;
  float: right;
  margin: 1ex 0 1.5ex;
  height: 30px;
  line-height: 30px;
  width: 100%;
}

#control div
{
  display: inline;
  padding-left: 1.5ex;
}

#control form
{
  display: inline;
}

#control #language
{
  border-right: 1px solid #CCCCCC;
  padding-right: 1.5ex;
}

#control #language input
{
  vertical-align: middle;
}

#control #language select
{
  width: 15ex;
}

#control #search
{
  margin-right: 2ex;
}

#control #search input
{
  border: 1px solid #888888;
}
/*** End: Header ***/

/*** Begin: Main section ***/
/* Two column layout based on http://positioniseverything.net/articles/onetruelayout/ */
#page /* This additional wrapper is to add space on the sides of the two columns (this might not be necessary- this is my first time using the One True Layout, so I'm not familar with other methods */
{
  background: #FFFFFF;
  padding-right: 3%; 
  padding-left: 3%;
  padding-top: 3ex;
  padding-bottom: 12ex;
  /* Added by Ramon , make Safari work */
  /* overflow: display; */
  overflow: auto; /* This is to clear the floats - will check in Safari as soon as possible */
  /* This might be for IE6- I forgot exactly why I added this. */
  /* overflow: hidden; */
  height: 1%;
  clear: both;
  width: 94%;
}

#main
{
  width: 70%;
  float: left;
}

#content
{
  float: left;
  width: 100%;
  color: #555555;
}

#content h2
{
  margin-top: 0.75ex;
}

#content p
{
  margin: 1.5ex 0;
}

#content ul
{
  padding-left: 3ex;
}

#sidebar
{
  float: left;
  width: 25%;
/*  margin-right: -185px; 160px + 25px = 185px */
  padding-left: 4%; /* This is a slight modification to add space between the columns. */
  padding-top: 2%;
}

#sidebar h2
{
  margin-top: 0.75ex;
  color: #444444;
  text-align: center;
  background: #DDDDDD;
  font-size: 1.7ex;
}

#portlet-gnome-sidenav
{
  width: 186px;
  background: url(&dtml-portal_url;/sidebar_top_cap.png) 0 0 no-repeat;
  padding-top: 16px;
}

#portlet-gnome-sidenav ul
{
  list-style: none;
  background: url(&dtml-portal_url;/sidebar_bottom_cap.png) 0 100% no-repeat;
  padding-bottom: 13px;
}

#portlet-gnome-sidenav li
{
  padding-bottom: 2px;
  background: url(&dtml-portal_url;/sidebar_item_divider.png) 0 100% no-repeat;
}

#portlet-gnome-sidenav a
{
  font-size: 2.3ex;
  text-indent: 2ex;
  color: #596159;
  text-decoration: none;
  display: block;
  background: url(&dtml-portal_url;/sidebar_item_bg.png) 0 0 repeat-y;
  padding-bottom: 2px;
}

#portlet-gnome-sidenav a:hover
{
  background: url(&dtml-portal_url;/sidebar_item_bg.png) 0 -78px repeat-y;
  color: #FFFFFF;
}

#breadcrumb
{
  border-top: 1px solid #DDDDDD;
  border-bottom: 1px solid #DDDDDD;
  background: #EEEEEE url(&dtml-portal_url;/breadcrumb.png) 2ex 50% no-repeat;
  padding: 0 2ex 0 5ex;
  line-height: 20px;
  height: 20px;
}

#breadcrumb a
{
  text-decoration: none;
  color: #446699;
}
/*** End: Main section ***/

/*** Begin: Footer ***/
#footer /* This is just thrown together based on the requirments at http://live.gnome.org/GnomeWeb/ComponentSelection */
{
  background: #FFFFFF url(&dtml-portal_url;/footer.png) 0 100% repeat-x;
  border-top: 1px solid #DDDDDD;
  padding: 2ex;
  clear: both;
  font-size: 1.6ex;
  text-align: center;
}

#portal-siteactions
{
  list-style: none;
}

#portal-siteactions li
{
  display: inline;
  border-left: 1px solid #AAAAAA;
}

#portal-siteactions li.first
{
  border-left: none;
}

#portal-siteactions li a
{
  text-decoration: none;
  color: #555555;
  margin: 0 1ex;
}

#portal-siteactions li a:hover
{
  color: #222222;
}

#portal-siteactions > li:first-child
{
    border-left: none !important;
}

/* User Actions */
#portal-personaltools, #portal-siteactions
{
  padding-top: 1em;
}

#portal-personaltools
{
  list-style: none;
}

#portal-personaltools li
{
  display: inline;
  border-left: 1px solid #AAAAAA;
}

#portal-personaltools li.first
{
  border-left: none;
}

#portal-personaltools li a
{
  text-decoration: none;
  color: #555555;
  margin: 0 1ex;
}

#portal-personaltools li a:hover
{
  color: #222222;
}

#portal-personaltools > li:first-child
{
    border-left: none !important;
}

/*** End: Footer ***/

/* Begin: PloneSoftwareCenter customization */
#psc-project-logo {
    display: block;
    width: 36px;
    height: 36px;
    float: left;
}

h1.pscTitle {
display: inline;
text-indent: auto;
color: black;
font-size: 280%;
}
#psc-content-wrapper {
display: block;
margin-left: 200px;
}
#psc-content-wrapper .documentDescription {
color: #888a85;
margin-top: 10px;
}
#psc-column {
display: block;
float: left;
width: 200px;
}
.psc-info {
display: block;
width: 190px;
margin-top: 10px;
margin-bottom: 10px;

}
.psc-info .psc-info-title {
font-weight: bold;
font-size: 120%;
display: block;
height: 35px;
padding: 15px 80px 0px 10px;
}
.psc-info .psc-info-body {
padding: 0px 10px 10px 10px;
}
.psc-info .psc-info-body ul {
list-style-type: none;
}
.psc-info .psc-info-body ul li {
clear: left;
margin-bottom: 2px;
margin-top: 2px;
}
.psc-info .psc-info-body ul li a {
display: block;
width: 130px;
border: 0px none;
}
.psc-info .psc-info-body ul li a img {
float: left;
margin-right: 3px;
margin-top: -2px;
}
#psc-categories {
background-image: url(&dtml-portal_url;/categories-box-top.png);
background-repeat: no-repeat;
}
#psc-categories .psc-info-body {
background-image: url(&dtml-portal_url;/categories-box-bottom.png);
background-repeat: no-repeat;
background-position: left bottom;
}
#psc-resources {
background-image: url(&dtml-portal_url;/resources-box-top.png);
background-repeat: no-repeat;
}
#psc-resources .psc-info-body {
background-image: url(&dtml-portal_url;/resources-box-bottom.png);
background-repeat: no-repeat;
background-position: left bottom;
}
#psc-project-screenshot {
float: right;
}
#psc-project-screenshot {
float: right;
width: 200px;
height: 140px;
text-align: center;
padding-left: 8px;
padding-top: 12px;
background-image: url(&dtml-portal_url;/screenshot-box.png);
background-repeat: no-repeat;
}
#psc-project-screenshot a {
float: none;
text-decoration: none;
}

/* End: PloneSoftwareCenter customization */



/* </dtml-with> (leave this one unmolested too :) */
