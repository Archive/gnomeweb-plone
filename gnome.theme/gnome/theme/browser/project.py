from Products.Five.browser import BrowserView
from Acquisition import aq_inner, aq_parent, aq_self

class ProjectGnomeView(BrowserView):
    """This code is copied from projectdoap.py inside PSC
    Copied here with duplication to avoid problem if the 
    version we are running is without DOAP support
    """
    def categories(self):
        """Get a list of categories url in a list, to be used for displaying the
        categories
        """
        categories = []
        
        parent = self.context.aq_inner.aq_parent.aq_self
        unfiltered_cats = parent.getField('availableCategories').getAsGrid(parent)
        
        for cat in unfiltered_cats:
            if cat[0] in self.context.getCategories():
                categories.append(
                    dict(id = cat[0],
                         url = "%s/by-category/%s" % ('/'.join(self.context.absolute_url().split('/')[:-1]), cat[0]),
                         name = cat[1],
                         desc = cat[2]
                        )
                )
    
        return categories
