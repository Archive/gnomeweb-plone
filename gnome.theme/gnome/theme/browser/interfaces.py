from plone.theme.interfaces import IDefaultPloneLayer
from zope.interface import Interface #, Attribute

class ISecondaryNavbar(Interface):

    def secondLevelNavLinks(actions=None, category='portal_tabs'):
        """Second level tabs
        """

class IThemeSpecific(IDefaultPloneLayer):
    """Marker interface that defines a Zope 3 layer.
       If you need to register a viewlet only for the
       "Gnome THeme" skin, this interface must be its layer
       (in theme/viewlets/configure.zcml).
    """
