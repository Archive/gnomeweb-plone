from zope.interface import implements
from Acquisition import aq_inner, aq_parent, aq_self

from Products.CMFPlone import utils
from Products.PloneSoftwareCenter.interfaces import ISoftwareCenterContent

from interfaces import ISecondaryNavbar

# This view is a copy/rewrite of 
# Products.CMFPlone.browser.navigation.CatalogNavigationTabs
class PSCCatalogSecondaryNavbar(utils.BrowserView):
    implements(ISecondaryNavbar)

    def secondLevelNavLinks(self, actions=None, category='portal_second_tabs'):
        # WARNING: the code commented below doesn't work at all, for undisclosed reasons:
        # reverting to "the simple solution" of just getting the parent
        context = utils.context(self)
        # Ok brutal check about where we are, if we're not in the softwarecenter root,
        # up one level
        #while not ISoftwareCenterContent.providedBy(context):
        #    context = context.aq_inner.aq_parent.aq_self
        if not ISoftwareCenterContent.providedBy(context):
            parent = context.aq_inner.aq_parent.aq_self
        else:
            parent = context
        # This is a very ugly hack to get the parent url: but apparently absolute_url gives
        # very inconsistent results (some times works, sometimes not)
        
        pTranslation = None
        if hasattr(parent, "getTranslation"):
            pTranslation = parent.getTranslation()
        if pTranslation:
            pPath = pTranslation.getPhysicalPath()
        else:
            pPath = parent.getPhysicalPath()
        parentPath = "/".join(pPath)
        portalPath = "/".join(context.portal_url.getPortalObject().getPhysicalPath()) + "/"
        parentPath = parentPath.replace(portalPath, "")
        
        result = []
        
        categories = parent.getField('availableCategories').getAsGrid(parent)
        # now add the content to results
        for category in categories:
            data = {'name'       : category[1],
                    'id'         : category[0],
                    'url'        : "%s/%s/by-category/%s" % (context.portal_url(), parentPath, category[0]),
                    'description': category[2]}
            result.append(data)
        return result

