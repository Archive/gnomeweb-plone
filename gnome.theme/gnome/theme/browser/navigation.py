from zope.interface import implements
from zope.component import getMultiAdapter

from Products.CMFCore.utils import getToolByName

from Products.CMFPlone import utils
from Products.CMFPlone import PloneMessageFactory as PMF
from Products.CMFPlone.browser.navtree import getNavigationRoot
from Products.CMFPlone.browser.navigation import get_view_url
from Products.CMFPlone.browser.interfaces import INavigationTabs, IPlone

from interfaces import ISecondaryNavbar

# See Products/gnometheme/overrides.zcml: This is an override of
# Products.CMFPlone.browser.navigation.CatalogNavigationTabs
class CatalogNavigationTabs(utils.BrowserView):
    implements(INavigationTabs)

    def topLevelTabs(self, actions=None, category='portal_tabs'):
        context = utils.context(self)

        portal_catalog = getToolByName(context, 'portal_catalog')
        portal_properties = getToolByName(context, 'portal_properties')
        navtree_properties = getattr(portal_properties, 'navtree_properties')
        site_properties = getattr(portal_properties, 'site_properties')

        # Build result dict
        result = []
        # first the actions
        if actions is not None:
            for actionInfo in actions.get(category, []):
                data = actionInfo.copy()
                # We use PMF instead of _() here, as this should not be picked
                # up by the extraction tool.
                data['name'] = PMF(data['title'], default=data['title'])
                result.append(data)

        # check whether we only want actions
        if site_properties.getProperty('disable_folder_sections', False):
            return result

        customQuery = getattr(context, 'getCustomNavQuery', False)
        if customQuery is not None and utils.safe_callable(customQuery):
            query = customQuery()
        else:
            query = {}

        rootPath = getNavigationRoot(context)
        portal_url = getToolByName(context, 'portal_url')
        portal = portal_url.getPortalObject()
        portalPath = portal_url.getPortalPath()
        contextPhysicalPath = context.getPhysicalPath()
        contextPath = '/'.join(contextPhysicalPath)
        # Obtain the language section 
        tempPath = contextPath.replace(portalPath, '')
        plone_view = getMultiAdapter((context, self.request),
                                     name='plone')
        if not plone_view.isPortalOrPortalDefaultPage():
            rootPath = "%s/%s" % (portalPath, tempPath.split('/')[1])
        query['path'] = {'query' : rootPath, 'depth' : 1}

        query['portal_type'] = utils.typesToList(context)

        sortAttribute = navtree_properties.getProperty('sortAttribute', None)
        if sortAttribute is not None:
            query['sort_on'] = sortAttribute

            sortOrder = navtree_properties.getProperty('sortOrder', None)
            if sortOrder is not None:
                query['sort_order'] = sortOrder

        if navtree_properties.getProperty('enable_wf_state_filtering', False):
            query['review_state'] = navtree_properties.getProperty('wf_states_to_show', [])

        query['is_default_page'] = False
        query['is_folderish'] = True

        # Get ids not to list and make a dict to make the search fast
        idsNotToList = navtree_properties.getProperty('idsNotToList', ())
        excludedIds = {}
        for id in idsNotToList:
            excludedIds[id]=1

        rawresult = portal_catalog.searchResults(query)

        # now add the content to results
        for item in rawresult:
            if not (excludedIds.has_key(item.getId) or item.exclude_from_nav):
                id, item_url = get_view_url(item)
                data = {'name'      : utils.pretty_title_or_id(context, item),
                        'id'         : id,
                        'url'        : item_url,
                        'description': item.Description}
                result.append(data)
        return result

# This view is a copy/rewrite of 
# Products.CMFPlone.browser.navigation.CatalogNavigationTabs
class CatalogSecondaryNavbar(utils.BrowserView):
    implements(ISecondaryNavbar)

    def secondLevelNavLinks(self, actions=None, category='portal_second_tabs'):
        context = utils.context(self)

        portal_catalog = getToolByName(context, 'portal_catalog')
        portal_properties = getToolByName(context, 'portal_properties')
        navtree_properties = getattr(portal_properties, 'navtree_properties')
        site_properties = getattr(portal_properties, 'site_properties')
        portal_url = getToolByName(context, 'portal_url')

        # Build result dict
        result = []

        # Start building the query
        query = {}

        portal = portal_url.getPortalObject()
        portalPath = portal_url.getPortalPath()
        contextPath = '/'.join(context.getPhysicalPath())
        # Obtain the section 
        tempPath = contextPath.replace(portalPath, '').split('/')
        if (len(tempPath)>2):
            section = "%s/%s" % (tempPath[1], tempPath[2])
        else:
            section = ''
        rootPath = "%s/%s" % (portalPath, section)

        # we still need to take care of default page for portal here
        if contextPath == portalPath:
            rootPath = portalPath
        query['path'] = {'query' : rootPath, 'depth' : 1}

        query['portal_type'] = utils.typesToList(context)

        sortAttribute = navtree_properties.getProperty('sortAttribute', None)
        if sortAttribute is not None:
            query['sort_on'] = sortAttribute

            sortOrder = navtree_properties.getProperty('sortOrder', None)
            if sortOrder is not None:
                query['sort_order'] = sortOrder

        if navtree_properties.getProperty('enable_wf_state_filtering', False):
            query['review_state'] = navtree_properties.getProperty('wf_states_to_show', [])

        query['is_default_page'] = False
        #query['is_folderish'] = True

        # Get ids not to list and make a dict to make the search fast
        idsNotToList = navtree_properties.getProperty('idsNotToList', ())
        excludedIds = {}
        for id in idsNotToList:
            excludedIds[id]=1

        rawresult = portal_catalog.searchResults(query)

        # now add the content to results
        for item in rawresult:
            if not (excludedIds.has_key(item.getId) or item.exclude_from_nav):
                id, item_url = get_view_url(item)
                data = {'name'      : utils.pretty_title_or_id(context, item),
                        'id'         : id,
                        'url'        : item_url,
                        'description': item.Description}
                result.append(data)
        return result

