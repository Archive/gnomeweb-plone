from zope.component import getMultiAdapter
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from plone.app.layout.viewlets.common import ViewletBase

from gnome.theme.config import DESIGNER

class CreditsViewlet(ViewletBase):
    render = ViewPageTemplateFile('credits.pt')

    def update(self):
        # set here the values that you need to grab from the template.
        # stupid example:
        self.designer = DESIGNER
