GLOBALS = globals()

DESIGNER = "Plone Gnome Group"

# CHANGE this tuple for modifying the list of portlets that will be displayed
# in the GNOME Theme.
# This list is here for keeping default right and left slots working in Plone
# Theme.
#   These are only for testing, they will be replaced by properly designed
#   slots from the mock-up
GNOMESLOTS = (
    'here/portlet_gnome_sidenav/macros/portlet',
    'here/portlet_review/macros/portlet',
    'here/portlet_gnome_example/macros/portlet',
    'here/portlet_calendar/macros/portlet',
    'here/portlet_events/macros/portlet',
    'here/portlet_recent/macros/portlet',
    )

# CHANGE this tuple of python dicts to set the actions to be added to the
# portal_actions tool.
# Gnome Theme Specific: Actions registered under the "gnome_top_links"
# category correspond to the links displayed at the very top of each page.
PORTALACTIONS = (
    {'id': 'gnome_home',
     'name': 'Home',
     'action': 'string:http://www.gnome.org/',
     'condition': '',
     'permission': 'View',
     'category': 'gnome_top_links',
     'visible': True,
     },
    {'id': 'gnome_news',
     'name': 'News',
     'action': 'string:http://news.gnome.org',
     'condition': '',
     'permission': 'View',
     'category': 'gnome_top_links',
     'visible': True,
     },
    {'id': 'gnome_projects',
     'name': 'Projects',
     'action': 'string:http://www.gnome.org/projects/',
     'condition': '',
     'permission': 'View',
     'category': 'gnome_top_links',
     'visible': True,
     },
    {'id': 'gnome_art',
     'name': 'Art',
     'action': 'string:http://art.gnome.org',
     'condition': '',
     'permission': 'View',
     'category': 'gnome_top_links',
     'visible': True,
     },
    {'id': 'gnome_support',
     'name': 'Support',
     'action': 'string:http://www.gnome.org/support/',
     'condition': '',
     'permission': 'View',
     'category': 'gnome_top_links',
     'visible': True,
     },
    {'id': 'gnome_development',
     'name': 'Development',
     'action': 'string:http://developer.gnome.org',
     'condition': '',
     'permission': 'View',
     'category': 'gnome_top_links',
     'visible': True,
     },
    {'id': 'gnome_community',
     'name': 'Community',
     'action': 'string:http://www.gnome.org/community/',
     'condition': '',
     'permission': 'View',
     'category': 'gnome_top_links',
     'visible': True,
     },
    )

# CHANGE this tuple of python dictionnaries to list the different skin
#  selections and their associated specific layers.
#   'name' (required): the name of the new skin.
#     This will be what the user sees when choosing skins, and will be the
#     name of a property in portal_skins.
#   'base' (required): the name of the skin selection on which the new one
#     is based.
#   'layers' (optional): the name of the specific layers for the skin
#     selection. By default (if the value is empty or if the key is absent
#     from the dictionnary), all the folders in 'skins/' will be listed
#     underneath 'custom' in the new skin selection layers.
SKINSELECTIONS = (
    {'name': "Gnome Theme",
     'base': "Plone Default",
     'layers': ( # order is important here, leave *_custom_* ones at the end
        'gnome_templates',
        'gnome_images',
        'gnome_styles',
        'gnome_custom_templates',
        'gnome_custom_images',
        'gnome_custom_styles',
        ),},
    )

# CHANGE this list to specify the names of the folders that should be
# registered as skin layers for all skin selections.
UNIVERSALLAYERS = (
    'gnome_templates',
    'gnome_images',
    'gnome_scripts',
    )

# CHANGE it to False if you don't want the new skin selection to be selected
#  at installation.
SELECTSKIN = True

# CHANGE it to the name of the skin selection that must be set as default in
#  case SELECTSKIN is set to True.
DEFAULTSKIN = "Gnome Theme"

# CHANGE this tuple of python dictionnaries to list the stylesheets that
#  will be registered with the portal_css tool.
#  'id' (required):
#    it must respect the name of the css or DTML file (case sensitive).
#    '.dtml' suffixes must be ignored.
#  'expression' (optional - default: ''): a tal condition.
#  'media' (optional - default: ''): possible values: 'screen', 'print',
#    'projection', 'handheld'...
#  'rel' (optional - default: 'stylesheet')
#  'title' (optional - default: '')
#  'rendering' (optional - default: 'import'): 'import', 'link' or 'inline'.
#  'enabled' (optional - default: True): boolean
#  'cookable' (optional - default: True): boolean (aka 'merging allowed')
#  See registerStylesheet() arguments in
#  ResourceRegistries/tools/CSSRegistry.py
#  for the latest list of all available keys and default values.
STYLESHEETS = (
    {'id': "gnome_public.css", 'media': 'screen', 'rendering': 'import'},
    )

# CHANGE this tuple of python dictionnaries to list the javascripts that
#  will be registered with the portal_javascripts tool.
#  'id' (required): same rules as for stylesheets.
#  'expression' (optional - default: ''): a tal condition.
#  'inline' (optional - default: False): boolean
#  'enabled' (optional - default: True): boolean
#  'cookable' (optional - default: True): boolean (aka 'merging allowed')
#  See registerScript() arguments in ResourceRegistries/tools/JSRegistry.py
#  for the latest list of all available keys and default values.
JAVASCRIPTS = (
#    {'id': "myjavascript.js.dtml",},
    )

# CHANGE it to True if you want users to be able to select the skin to use
#  from their personal preferences management page.
#  In the ZMI, this value is known as 'Skin flexibility'.
ALLOWSELECTION = False

# CHANGE it to True if you want to make the skin cookie persist indefinitely.
#  In the ZMI, this value is known as 'Skin Cookie persistence'.
PERSISTENTCOOKIE = False

# CHANGE it to True if you want portal_skins properties to be reset to Plone
#  default values when the product is uninstalled:
#  Default Skin: "Plone Default", Skin flexibility: False,
#  Skin Cookie persistence: False.
#  If set to False: Default Skin: BASESKIN,
#  Skin flexibility and Skin Cookie persistence left unmolested.
RESETSKINTOOL = False
