from setuptools import setup, find_packages
import sys, os

version = '0.1'

setup(name='gnome.theme',
      version=version,
      description="",
      long_description="""\
""",
      # Get more strings from http://www.python.org/pypi?%3Aaction=list_classifiers
      classifiers=[
        "Framework :: Plone",
        "Framework :: Zope2",
        "Framework :: Zope3",
        "Programming Language :: Python",
        "Topic :: Software Development :: Libraries :: Python Modules",
        ],
      keywords='',
      author='Plone Gnome Group',
      author_email='gnomeweb-list@gnome.org',
      url='http://svn.gnome.org/svn/gnomeweb-plone/trunk/gnome.theme',
      license='GPL',
      packages=find_packages(exclude=['ez_setup']),
      namespace_packages=['gnome'],
      include_package_data=True,
      zip_safe=False,
      install_requires=[
          'setuptools',
          # -*- Extra requirements: -*-
      ],
      entry_points="""
      # -*- Entry points: -*-
      """,
      )
