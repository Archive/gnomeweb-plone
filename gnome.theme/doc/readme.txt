Description

    gnometheme is a product that adds a new theme to a Plone portal.
    It requires Plone 2.1.x or Plone 2.5.x.

    gnometheme is based on DIYPloneStyle 2.1.3, a skeleton product
    ready for building new graphical designs for Plone.

Installation

    Place gnometheme in the Products directory of your Zope instance
    and restart the server.

    Go to the 'Site Setup' page in the Plone interface and click on the
    'Add/Remove Products' link.

    Choose 'Gnome Theme' (check its checkbox) and click the 'Install' button.

    You may have to empty your browser cache to see the effects of the
    product installation/uninstallation.

    Uninstall -- This can be done from the same management screen.

Selecting a skin

   Depending on the value given to SELECTSKIN (in config.py), the skin will be
   selected (or not) as default one while installing the product. If you need
   to switch from a default skin to another, go to the 'Site Setup' page, and
   choose 'Skins' (as portal manager). You can also decide from that page if
   members can choose their preferred skin and, in that case, if the skin
   cookie should be persistent.

   Note -- Don't forget to perform a full refresh of the page or reload all
   images (not from browser cache) after selecting a skin.
   In Firefox, you can do so by pressing the 'shift' key while reloading the
   page. In IE, use the key combination <Ctrl-F5>.

Copyright 2006

    The GNOME Project <gnome-web-list@gnome.org>

Contributors

    * Ramon Navarro Bosch   ramon_at_epsem_upc_edu

    * Maria Soler           maria.soler_at_eupm_upc_edu

    * David Convent         davconvent_at_gmail_com
