Tasks should be listed here with the name of developer.

Whenever you decide to tackle a new development for the Gnome Theme, please
add in the list so that other contributors know what's going on.

Once a task is achieved, it should be summarized in the HISTORY.txt file and
removed from the list.

Task List:

* Write Unit Tests !!


