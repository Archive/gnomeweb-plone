from setuptools import setup, find_packages
import os

version = '1.0'

setup(name='wgo.theme',
      version=version,
      description="The Visual design for Gnome Web on Plone",
      long_description=open("README.txt").read() + "\n" +
                       open(os.path.join("docs", "HISTORY.txt")).read(),
      # Get more strings from http://www.python.org/pypi?%3Aaction=list_classifiers
      classifiers=[
        "Framework :: Plone",
        "Programming Language :: Python",
        "Topic :: Software Development :: Libraries :: Python Modules",
        ],
      keywords='web zope plone theme',
      author='Ramon Navarro Bosch',
      author_email='ramon.nb@gmail.com',
      url='http://svn.gnome.org/svn/gnomeweb-plone',
      license='GPL',
      packages=find_packages(exclude=['ez_setup']),
      namespace_packages=['wgo'],
      include_package_data=True,
      zip_safe=False,
      install_requires=[
          'setuptools',
          # -*- Extra requirements: -*-
      ],
      entry_points="""
      # -*- Entry points: -*-
      """,
      )
