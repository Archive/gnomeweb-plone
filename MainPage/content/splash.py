# -*- coding: utf-8 -*-
#
# File: Splash.py
#
# Copyright (c) 2007 by []
# Generator: ArchGenXML Version 1.5.1-svn
#            http://plone.org/products/archgenxml
#
# GNU General Public License (GPL)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.
#

__author__ = """Ramon Navarro Bosch <ramon@headnet.dk>"""
__docformat__ = 'plaintext'

from AccessControl import ClassSecurityInfo
from Products.Archetypes.atapi import *
from Products.MainPage.config import *

schema = Schema((

    TextField(
        name='body',
        allowable_content_types=('text/plain', 'text/structured', 'text/html', 'application/msword',),
        widget=RichWidget(
            label='Body',
            label_msgid='MainPage_label_body',
            i18n_domain='MainPage',
        ),
        default_output_type='text/html'
    ),

),
)

Splash_schema = BaseSchema.copy() + \
    schema.copy()

class Splash(BaseContent):
    """ splash class for showing the images
    """

    _at_rename_after_creation = True
    schema = Splash_schema

    # Methods


registerType(Splash, PROJECTNAME)
# end of class Splash

