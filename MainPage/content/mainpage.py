# -*- coding: utf-8 -*-
#
# GNU General Public License (GPL)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.
#

__author__ = """Ramon Navarro Bosch <ramon@headnet.dk>"""
__docformat__ = 'plaintext'

from AccessControl import ClassSecurityInfo
from Products.Archetypes.atapi import *
from Products.MainPage.config import *

from Products.ATContentTypes.content.folder import  ATFolder, ATFolderSchema
from Products.MainPage.interfaces import IMainPage

from zope.interface import implements

schema = Schema((


    ReferenceField(
        name='pscprojects',
        widget=ReferenceWidget(
            label='Pscprojects',
            label_msgid='MainPage_label_pscprojects',
            i18n_domain='MainPage',
        ),
        allowed_types=('PSCProject',),
        multiValued=0,
        relationship='mainpageconfig_pscproject'
    ),


    TextField(
        name='involved',
        required=True,
        allowable_content_types=('text/plain', 'text/structured', 'text/html', 'application/msword',),
        widget=RichWidget(
            label='Involved',
            label_msgid='MainPage_label_involved',
            i18n_domain='MainPage',
        ),
        default_output_type='text/html'
    ),

    TextField(
        name='slogan',
        required=True,
        allowable_content_types=('text/plain', 'text/structured', 'text/html', 'application/msword',),
        widget=RichWidget(
            label='Slogan',
            label_msgid='MainPage_label_slogan',
            i18n_domain='MainPage',
        ),
        default_output_type='text/html'
    ),

),
)


MainPage_schema = ATFolderSchema.copy() + \
    schema.copy()


class MainPage(ATFolder):
    """
    """

    implements(IMainPage)

    _at_rename_after_creation = True
    schema = MainPage_schema


registerType(MainPage, PROJECTNAME)

