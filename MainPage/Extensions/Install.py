from StringIO import StringIO
from Products.CMFCore import utils as cmfutils
from Products.CMFPlone.utils import getToolByName

def setup_gs_profile(portal, out):
    setup_tool = cmfutils.getToolByName(portal, 'portal_setup')
    for extension_id in ['Products.MainPage:default']:
        try:
            setup_tool.setImportContext('profile-%s' % extension_id)
            setup_tool.runAllImportSteps()
        except Exception, e:
            print >> out, "error while trying to GS import %s (%s, %s)" \
                          % (extension_id, repr(e), str(e))
    setup_tool.setImportContext('profile-Products.MainPage:default')


def install(portal):
    out = StringIO()
    setup_gs_profile(portal, out)

def uninstall(self, reinstall=False):
    pass

