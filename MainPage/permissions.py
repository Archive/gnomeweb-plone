from AccessControl.Permissions import add_user_folders as AddUserFolders
from Products.CMFCore.permissions import setDefaultRoles

# Basic permissions
from Products.CMFCore.permissions import View
from Products.CMFCore.permissions import ModifyPortalContent
from Products.CMFCore.permissions import AddPortalContent
from Products.CMFCore.permissions import AccessContentsInformation
from Products.CMFCore.permissions import ListFolderContents
from Products.CMFCore.permissions import SetOwnPassword as SetPassword
from Products.CMFCore.permissions import ManageUsers

# Add permissions
AddMainpage = "mainpage: Add MainPage"
AddBanner = "mainpage: Add Banner"
AddSplash = "mainpage: Add Splash"

setDefaultRoles(AddMainpage, ('Manager',))
setDefaultRoles(AddBanner, ('Member',))
setDefaultRoles(AddSplash, ('Member',))

DEFAULT_ADD_CONTENT_PERMISSION = AddPortalContent
ADD_CONTENT_PERMISSIONS = {
    'MainPage'    : AddMainpage,
    'Banner'      : AddBanner,
    'Splash'      : AddSplash,
}
