from Products.Five.browser import BrowserView
from Products.CMFCore.utils import getToolByName


import urllib
import libxml2
import random

class MainPageView(BrowserView):
    """ The view for the Main Page """

    def __init__(self, context, request):
        self.context = context
        self.request = request

    def get_news(self):
        """ Search on the Web """
        #portal_url = getToolByName(context, 'portal_url')
        #portal = portal_url.getPortalObject()
        #url = portal.portal_mainpageconfig.getRssnews()
        #import pdb; pdb.set_trace()
        url = getToolByName(self.context, 'portal_properties')['MainPage_properties'].rssnews
        try:
             f = urllib.urlopen(url)
             xmldoc = f.read()
             doc = libxml2.parseDoc(xmldoc)
        except:
             return {'error': 1, 'message': ["Could not open url"]}
        ctxt = doc.xpathNewContext()
        res = ctxt.xpathEval("//item")
        news = []
        for result in res:
            child = result.children
            while child is not None:
                if child.type == "element":
                    if child.name == "title":
                        title = child.content
                    if child.name == "link":
                        link = child.content
                    if child.name == "description":
                        description = child.content
                    if child.name == "pubDate":
                        pubdate = child.content
                    if child.name == "category":
                        category = child.content
                child = child.next
            news.append({'title':title,'link':link,'description':description,'pubdate':pubdate,'category':category})
        return {'error': 0, 'news':news}


    def get_planet(self):
        """ Search on the Web for the Planet """
        #portal_url = getToolByName(context, 'portal_url')
        #portal = portal_url.getPortalObject()
        #url = portal.portal_mainpageconfig.getRssplanet()
        #import pdb; pdb.set_trace()
        url = getToolByName(self.context, 'portal_properties')['MainPage_properties'].rssplanet
        try:
             f = urllib.urlopen(url)
             xmldoc = f.read()
             doc = libxml2.parseDoc(xmldoc)
        except:
             return {'error': 1, 'message': ["Could not open url"]}
        ctxt = doc.xpathNewContext()
        res = ctxt.xpathEval("//entry")
        planet = []
        for result in res:
            child = result.children
            while child is not None:
                if child.type == "element":
                    if child.name == "title":
                        title = child.content
                    if child.name == "link":
                        link = child.content
                    if child.name == "id":
                        ident = child.content
                    if child.name == "updated":
                        updated = child.content
                    if child.name == "content":
                        content = child.content
                    if child.name == "author":
                        child2 = child.children
                        while child2 is not None:
                            if child2.type == "element":
                                if child2.name == "name":
                                    author_name = child2.content
                                if child2.name == "uri":
                                    author_url = child2.content
                            child2 = child2.next
                    if child.name == "source":
                        child2 = child.children
                        while child2 is not None:
                            if child2.type == "element":
                                if child2.name == "title":
                                    source_title = child2.content
                                if child2.name == "link":
                                    source_link = child2.content
                                if child2.name == "id":
                                    source_id = child2.content
                                if child2.name == "updated":
                                    source_updated = child2.content
                            child2 = child2.next
                child = child.next
            author = {'name':author_name,'uri':author_uri}
            source = {'title':source_title,'link':source_link,'id':source_id,'updated':source_updated}
            planet.append({'title':title,'link':link,'id':ident,'updated':updated,'content':content,'author':author,'source':source})
        return {'error': 0, 'planet':planet}

    def get_slogan(self):
        return self.context.getSlogan()

    def get_involved(self):
        return self.context.getInvolved()

    def get_banner(self):
        """ Search for banners """
        banners = self.context.portal_catalog(portal_type=['Banner'],path='/',review_state=['published'])
        if len(banners)>0:
            f=random.choice(banners).getObject()
            return f.getText()
        else:
            return "<div>Empty banner</div>"

    def get_splash(self):
        """ Search for splash """
        splashs = self.context.portal_catalog(portal_type=['Splash'],path='/',review_state=['published'])
        if len(splashs)>0:
            f=random.choice(splashs).getObject()
            return f.getBody()
        else:
            return "<div>empty splash</div>"
        

    def get_events(self):
        """ Search for events """
        events = "<div>emprty event</div>"
        return events

    def get_product(self):
        """ Search for the product and generate the html code """
        return "<div>empty product</div>"
