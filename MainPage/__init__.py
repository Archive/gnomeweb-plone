from Products.CMFCore.utils import ContentInit
from Products.CMFCore.permissions import AddPortalContent
from Products.CMFCore.DirectoryView import registerDirectory

from Products.Archetypes import atapi

from Products.CMFPlone.interfaces import IPloneSiteRoot
from Products.GenericSetup import EXTENSION, profile_registry

from permissions import DEFAULT_ADD_CONTENT_PERMISSION, ADD_CONTENT_PERMISSIONS

from config import PROJECTNAME, GLOBALS


registerDirectory('skins', GLOBALS)

def initialize(context):

    # Register Archetypes content types

    import content

    contentTypes, constructors, ftis = atapi.process_types(atapi.listTypes(PROJECTNAME), PROJECTNAME)

    ContentInit(
        PROJECTNAME + ' Content',
        content_types      = contentTypes,
        permission         = DEFAULT_ADD_CONTENT_PERMISSION,
        extra_constructors = constructors,
        fti                = ftis,
        ).initialize(context)


